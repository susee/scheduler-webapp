import { Component } from '@angular/core';
@Component({
  selector: 'my-app',
  template: '<h1>{{title}}</h1><h2>Scheduled meetings for {{user}}:</h2>'
})
export class AppComponent {
	title = 'Welcome to Outlook Scheduler Application';
	user = 'Suseella';
 }

